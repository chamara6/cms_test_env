---
title: new honshabun plus mark
layout: full-width
categories:
  - new
date: 2022-11-20T10:28:53.795Z
---
<html><head><meta charset="UTF-8"><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta http-equiv="X-UA-Compatible" content="ie=edge"><meta name="viewport" content="width=device-width, initial-scale=1.0"><link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet" /><link href="https://cdn.jsdelivr.net/npm/tailwindcss/dist/tailwind.min.css" rel="stylesheet"><script src="https://cdn.tailwindcss.com"></script><script src="https://unpkg.com/tailwindcss-jit-cdn"></script></head>

<body>

<img src="/images/10.png" width="800" height="300"></img>

<h1 class="black-600 text-right text-xs"> 🔄 Update：2022/10/19</h1>

<h1 class="text-blue-600 text-center font-bold text-2xl">本社部門紹介</h1>

</div>

<br>

<hr class="border-dashed border-black "></hr><script src="https://cdn.tailwindcss.com"></script><style>/* Tab content - closed */.tab-content {max-height: 0;-webkit-transition: max-height .35s;-o-transition: max-height .35s;transition: max-height .35s;}/* :checked - resize to full height */.tab input:checked~.tab-content {max-height: 200vh;}/* Icon */.tab label::after {float: right;right: 0;top: 0;display: block;width: 1.5em;height: 1.5em;line-height: 1.5;font-size: 1.25rem;text-align: center;-webkit-transition: all .35s;-o-transition: all .35s;transition: all .35s;}/* Icon formatting - closed */.tab input[type=checkbox]+label::after {content: "+";font-weight: bold;/*.font-bold*//*.border*/border-radius: 9999px;/*.rounded-full */border-color: #b8c2cc;/*.border-grey*/}.tab input[type=radio]+label::after {content: "\25BE";font-weight: bold;/*.font-bold*//*.border*/border-radius: 9999px;/*.rounded-full */border-color: #b8c2cc;/*.border-grey*/}/* Icon formatting - open */.tab input[type=checkbox]:checked+label::after {transform: rotate(315deg);/*.bg-indigo*/color: #f8fafc;/*.text-grey-lightest*/}</style></head>

<br>

<div class="">

<div class="tab w-full overflow-hidden border-t">

<div class=" flex flex-col space-y-4">

<div class="tab w-full text-xl text-black overflow-hidden border-t"><input class="absolute opacity-0 " id="tab-multi-one" type="checkbox" name="tabs"><label class="block p-2 text-sm text-left rounded-md bg-green-500 leading-normal cursor-pointer" for= "tab-multi-one"><b>事業企画室</b><span class="text-red-500 text-base font-bold">　New!!</span></label><div class="tab-content overflow-hidden border-l-5 leading-normal"><div class="cp_actab-content"><p><a name="unei1" id="uneikanri" class="mce-item-anchor"></a></p>

</div>

<br>

<div class="bg-blue-300 bg-opacity-50 p-2 w-full h-full ml-2">

<span class="text-black-600 text-left text-base text-xl font-bold" >今月のニュース</sapn></div><br>

<!--StartFragment-->

    <div class="mx-auto overflow-hidden">

        <div class="flex flex-row flex-nowrap">

          <div class="md:shrink-0">

            <figure class="p-8 h-32 w-32"><img src="/images/s1.png" class="rounded-full border-2 border-sky-500" /><figcaption class="text-center">金子年長</figcaption></figure>

        </div>

        <div class="">

          <div class="p-5 rounded-md bg-blue-300 bg-opacity-50 text-black">

            <p class="text-xs"> スマート介護士という資格をご存じですか？少ない人員で効率的な業務遂行がもとめられるなか、DX化を図り介護の質の向上と効率化を実行できる介護士が「スマート介護士」です。興味があれば事業企画室までお問合わせ下さい。</p>

          </div>

        </div>

    </div><br>

<div class="bg-blue-300 bg-opacity-50 p-2 w-full h-full ml-2">

<span class="text-black-600 text-left text-base text-xl font-bold">お問い合わせ</sapn></div><br>

<!--StartFragment-->

    <div class="mx-auto overflow-hidden">

        <div class="flex flex-row flex-nowrap">

          <div class="md:shrink-0">

            <figure class="p-8 h-32 w-32"><img src="/images/s2.png" class="rounded-full border-2 border-sky-500" /><figcaption class="text-center">金子年長</figcaption></figure>

</figure>

        </div>

 <div class="p-2 rounded-md bg-blue-300 flex-initial w-100 h-20 bg-opacity-50 text-black">

            <p class="text-lg"> こんなことに困ったらお問い合わせください！</p>

          </div>

        </div>

      </div>

    </div>

<p class="text-center ...">問い合わせ先<br> ✉home-Test@Test.jp<br> 📞03-633-</p></div>

<br>

<div class="">

<div class="tab w-full overflow-hidden border-t"> 

<div class=" flex flex-col space-y-4">

<div class="tab w-full  text-xl text-black overflow-hidden border-t">

<input class="absolute opacity-0 " id="tab-multi-two" type="checkbox" name="tabs">

<label class="block p-2 text-sm text-left rounded-md bg-green-500   leading-normal cursor-pointer " for="tab-multi-two"><b>開発部</b><span class="text-red-500 text-base font-bold"></label>

<div class="tab-content overflow-hidden border-l-5  leading-normal">

<div class="cp_actab-content">

<p><a name="unei1" id="uneikanri" class="mce-item-anchor"></a></p>

</div>

<br>

<div class="bg-blue-300 bg-opacity-50 p-2 w-full h-full ml-2">

<span class="text-black-600 text-left text-base text-xl font-bold">仕事内容</sapn></div><br>

<div class="bg-amber-200  text-align-left text-left bg-opacity-100 p-2 ml-8"><span class="<text-xl text-left text-black  ml-2 font-bold "></span>その名の通り、施設の開発を行うのが主な仕事です。そのためには不動産知識も必要ですが昨今は、何よりも開設後のサービスオペレーションをイメージした施設づくりが重要になります。不動産のみならず現場サービスの知識も必要になってきます。</span></div><br>

<div class="bg-blue-300 bg-opacity-50 p-2 w-full h-full ml-2">

<span class="text-black-600 text-left text-base text-xl font-bold">お問い合わせ</sapn></div><br>

<div class="mx-auto overflow-hidden">

        <div class="flex flex-row flex-nowrap">

          <div class="md:shrink-0">

     <figure class="p-8 h-32 w-32"><div class="balloon5"><div class="faceicon"><img src="/images/s3.png" class="rounded-full border-2 border-sky-500" /><figcaption class="text-center">金子年長</figcaption></figure>

        </div>

        <div class="">

          <div class="p-5 rounded-md bg-blue-300 bg-opacity-50 text-black">

            <p class="text-lg"> 困ったらお問い合わせください！</p>

          </div>

        </div>

      </div>

    </div>

<p class="text-center ...">問い合わせ先<br> ✉home-Test@Test.jp<br> 📞03-633-</p></div>

<br>