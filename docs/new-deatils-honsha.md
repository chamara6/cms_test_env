---
title: new-deatils-honsha
layout: full-width
categories:
  - new
date: 2022-11-21T23:57:50.456Z
---
<html>

  <head>

    <script src="https://cdn.tailwindcss.com">

    </script>

  </head>

  <body>

    <!-- content -->

<img src="/images/10.png" width="800" height="300"></img>

<h1 class="black-600 text-right text-xs"> 🔄 Update：2022/10/19</h1>

<h1 class="text-blue-600 text-center font-bold text-2xl">本社部門紹介</h1>
           <hr>

  </div>

<br>

<hr class="border-dashed border-black "></hr>

<head> <title>Tailwind CSS Accordion</title><script src="https://cdn.tailwindcss.com"></script><link href="https://cdn.jsdelivr.net/npm/tailwindcss/dist/tailwind.min.css" rel="stylesheet"> <style> /* Tab content - closed */.tab-content { max-height: 0; -webkit-transition: max-height .35s; -o-transition: max-height .35s; transition: max-height .35s; } /* :checked - resize to full height */ .tab input:checked ~ .tab-content { max-height: 100vh; } /* Label formatting when open */ { /*@apply text-xl p-5 border-l-2 border-indigo-500 bg-gray-100 text-indigo*/ font-size: 1.25rem; /*.text-xl*/ padding: 1.25rem; /*.p-5*/ border-left-width: 2px; /*.border-l-2*/ border-color: #6574cd; /*.border-indigo*/ background-color: #f8fafc; /*.bg-gray-100 */ color: #6574cd; /*.text-indigo*/ } /* Icon */ .tab label::after { float:right; right: 0; top: 0; display: block; width: 1.5em; height: 1.5em; line-height: 1.5; font-size: 1.25rem; text-align: center; -webkit-transition: all .35s; -o-transition: all .35s; transition: all .35s; } /* Icon formatting - closed */ .tab input[type=checkbox] + label::after { content: "+"; font-weight:bold; /*.font-bold*/  /*.border*/ border-radius: 9999px; /*.rounded-full */ border-color: #b8c2cc; /*.border-grey*/ } .tab input[type=radio] + label::after { content: "\25BE"; font-weight:bold; /*.font-bold*//*.border*/ border-radius: 9999px; /*.rounded-full */ border-color: #b8c2cc; /*.border-grey*/ } /* Icon formatting - open */ .tab input[type=checkbox]:checked + label::after { transform: rotate(315deg); background-color: #6574cd; /*.bg-indigo*/ color: #f8fafc; /*.text-grey-lightest*/ } .tab input[type=radio]:checked + label::after { transform: rotateX(180deg); background-color: #6574cd; /*.bg-indigo*/ color: #f8fafc; /*.text-grey-lightest*/ } </style> </head> 

</div>

<br>

<div class="">

<div class="tab w-full overflow-hidden border-t"> 

<div class=" flex flex-col space-y-4">

<div class="tab w-full  text-xl text-black overflow-hidden border-t">

<input class="absolute opacity-0 " id="tab-multi-one" type="checkbox" name="tabs">

<label class="block p-2 text-sm text-left rounded-md bg-green-500   leading-normal cursor-pointer " for="tab-multi-one"><b>事業企画室</b><span class="text-red-500 text-base font-bold">　New!!</span></label>

<div class="bg-blue-300 bg-opacity-50 p-2 w-full h-full ml-2">

<span class="text-black-600 text-left text-base text-xl font-bold">仕事内容</span></div><br>

<div class="bg-amber-200  text-align-left text-left bg-opacity-100 p-2 ml-8"><span class="text-xl text-left text-black  underline underline-offset-4 ml-2 font-bold ">■Care Innovation Team<br></span><span class="text-xm">介護現場の抱える課題（サービス品質・人員確保・情報共有など）についてテクノロジーを活用した解決策を立案し、導入から活用支援までを行っております。また企業と協働で新たな技術開発も行なっています。</span></div><br>

<div class="bg-amber-200  text-align-left text-left bg-opacity-100 p-2 ml-8"><span class="text-xl text-left text-black  underline underline-offset-4 ml-2 font-bold ">■Public Relations Team<br></span><span class="text-xm">HITOWAケアサービスとイリーゼの（ブランド）価値を、進化するメディアに対応しながら魅力的に発信していきます。</span></div>

</div>

<br>

<div style="background: #c6e6f5; padding: 10px;" data-mce-style="background: #c6e6f5; padding: 10px;"><span style="font-size: 18px;" data-mce-style="font-size: 18px;"><strong>今月のニュース</strong></span></div><!--吹き出しはじまり--><br>

<!--StartFragment-->

    <div class="mx-auto overflow-hidden">

        <div class="flex flex-row flex-nowrap">

          <div class="md:shrink-0">

            <figure class="p-8 h-32 w-32"><img src="https://image.jimcdn.com/app/cms/image/transf/none/path/s96da70f606bae585/image/if1f118f923f4dcea/version/1556099238/image.jpg" class="rounded-full border-2 border-sky-500" /><figcaption class="text-center">金子年長</figcaption></figure>

        </div>

        <div class="">

          <div class="p-5 rounded-md bg-blue-300 bg-opacity-50 text-black">

            <p class="text-lg"> スマート介護士という資格をご存じですか？少ない人員で効率的な業務遂行がもとめられるなか、DX化を図り介護の質の向上と効率化を実行できる介護士が「スマート介護士」です。興味があれば事業企画室までお問合わせ下さい。</p>

          </div>

        </div>

      </div>

    </div><br>

<div style="background: #c6e6f5; padding: 10px;" data-mce-style="background: #c6e6f5; padding: 10px;"><span style="font-size: 18px;" data-mce-style="font-size: 18px;"><strong>お問い合わせ</strong></span></div><!--吹き出しはじまり--><br>

<!--StartFragment-->

    <div class="mx-auto overflow-hidden">

        <div class="flex flex-row flex-nowrap">

          <div class="md:shrink-0">

            <figure class="p-8 h-32 w-32"><img src="https://image.jimcdn.com/app/cms/image/transf/dimension=1920x400:format=jpg/path/s96da70f606bae585/image/i5556d65a7fddf0af/version/1559547102/image.jpg" class="rounded-full border-2 border-sky-500" /><figcaption class="text-center">金子年長</figcaption>

<div class="chatting"><div class="says">こんなことに困ったらお問い合わせください！</div></div>

</figure>

        </div>

        <div class="">

          <div class="p-5 rounded-md bg-blue-300 bg-opacity-50 text-black">

            <p class="text-lg">こんなことに困ったらお問い合わせください！</p>

          </div>

        </div>

      </div>

    </div>

<p style="text-align: center;" data-mce-style="text-align: center;"><span style="font-size: 16px;" data-mce-style="font-size: 16px;">問い合わせ先</span><br> ✉home-Test@Test.jp<br> 📞03-633-</p></div></div>

2﻿22222222222222222222222222222

<div class="">

<div class="tab w-full overflow-hidden border-t"> 

<div class=" flex flex-col space-y-4">

<div class="tab w-full  text-xl text-black overflow-hidden border-t">

<input class="absolute opacity-0 " id="tab-multi-two" type="checkbox" name="tabs">

<label class="block p-2 text-sm text-left rounded-md bg-green-500   leading-normal cursor-pointer " for="tab-multi-two"><b>開発部</b><span class="text-red-500 text-base font-bold">　New!!</span></label>

<div class="tab-content overflow-hidden border-l-5  leading-normal">

<div class="cp_actab-content">

<p><a name="unei1" id="uneikanri" class="mce-item-anchor"></a></p>

</div>

<br>



<div class="bg-blue-300 bg-opacity-50 p-2 w-full h-full ml-2">

<span class="text-black-600 text-left text-base text-xl font-bold">仕事内容</sapn></div><br>

<div class="bg-amber-200  text-align-left text-left bg-opacity-100 p-2 ml-8"><span class="<text-xl text-left text-black  ml-2 font-bold "></span>その名の通り、施設の開発を行うのが主な仕事です。そのためには不動産知識も必要ですが昨今は、何よりも開設後のサービスオペレーションをイメージした施設づくりが重要になります。不動産のみならず現場サービスの知識も必要になってきます。</span></div><br>

<div class="bg-blue-300 bg-opacity-50 p-2 w-full h-full ml-2">

<span class="text-black-600 text-left text-base text-xl font-bold">お問い合わせ</sapn></div><br>

<div class="mx-auto overflow-hidden">

        <div class="flex flex-row flex-nowrap">

          <div class="md:shrink-0">

     <figure class="p-8 h-32 w-32"><div class="balloon5"><div class="faceicon"><img src="/images/s3.png" class="rounded-full border-2 border-sky-500" /><figcaption class="text-center">金子年長</figcaption></figure>

        </div>

        <div class="">

          <div class="p-5 rounded-md bg-blue-300 bg-opacity-50 text-black">

            <p class="text-lg"> 困ったらお問い合わせください！</p>

          </div>

        </div>

      </div>

    </div>

<p class="text-center ...">問い合わせ先<br> ✉home-Test@Test.jp<br> 📞03-633-</p></div></div>

3﻿3333333333333333333333333

<div class="">

<div class="tab w-full overflow-hidden border-t"> 

<div class=" flex flex-col space-y-4">

<div class="tab w-full  text-xl text-black overflow-hidden border-t">

<input class="absolute opacity-0 " id="tab-multi-three" type="checkbox" name="tabs">

<label class="block p-2 text-sm text-left rounded-md bg-green-500   leading-normal cursor-pointer " for="tab-multi-three"><b>営業部</b><span class="text-red-500 text-base font-bold">　New!!</span></label>

<div class="tab-content overflow-hidden border-l-5  leading-normal">

<div class="cp_actab-content">

<p><a name="unei1" id="uneikanri" class="mce-item-anchor"></a></p>

</div>

<br>

<div class="bg-blue-300 bg-opacity-50 p-2 w-full h-full ml-2">

<span class="text-black-600 text-left text-base text-xl font-bold">仕事内容</sapn></div><br>

<div class="bg-amber-200  text-align-left text-left bg-opacity-100 p-2 ml-8"><span class="<text-xl text-left text-black  underline underline-offset-4 ml-2 font-bold ">■営業促進課<br></span>ご入居を検討されているお客様の誘致ならびにご入居までのご案内を実施しています</span></div><br>

<div class="bg-amber-200  text-align-left text-left bg-opacity-100 p-2 ml-8"><span class="<text-xl text-left text-black  underline underline-offset-4 ml-2 font-bold ">■営業促進課<br></span>ご入居を検討されているお客様の誘致ならびにご入居後のお客様<wbr>相談窓口として活動しています</span></div><br>

<div class="bg-amber-200  text-align-left text-left bg-opacity-100 p-2 ml-8"><span class="<text-xl text-left text-black  underline underline-offset-4 ml-2 font-bold ">■営業促進課<br></span>マーケティングによる営業活動と法人取引の新規開拓及び管理業務を行っています。</span></div><br>

<div style="background: #c6e6f5; padding: 10px;" data-mce-style="background: #c6e6f5; padding: 10px;"><span style="font-size: 18px;" data-mce-style="font-size: 18px;"><strong>お問い合わせ</strong></span></div><br>

<div class="mx-auto overflow-hidden">

        <div class="flex flex-row flex-nowrap">

          <div class="md:shrink-0">

            <figure class="p-8 h-32 w-32"><div class="balloon5"><div class="faceicon"><div class="balloon5"><div class="faceicon"><img src="https://image.jimcdn.com/app/cms/image/transf/dimension=1920x400:format=jpg/path/s96da70f606bae585/image/ibfb0f2e1aa2af280/version/1556067908/image.jpg"  class="rounded-full border-2 border-sky-500" /><figcaption class="text-center">金子年長</figcaption></figure>

        </div>

        <div class="">

          <div class="p-5 rounded-md bg-blue-300 bg-opacity-50 text-black">

            <p class="text-lg">こんなことに困ったらお問い合わせください！</p>

          </div>

        </div>

      </div>

    </div><br>

<div class="border-2 border-gray-300 rounded-md outline- 4 p-2 "><ul class="list-disc list-inside "> <li>ご入居を検討している方を紹介したい</li> <li>紹介業を検討している事業者を紹介したい</li> <li>ご入居を検討している方を紹介したい</li> <li>有料サービスの運用ルール、お客様へのサービス内容について知りたい</li> <li>介護の事でお悩みの方の相談に乗ってもらいたい</li></ul><br></div></input><br>

<p class="text-center ...">問い合わせ先<br> ✉home-Test@Test.jp<br> 📞03-633-</p></div></div>

4﻿444444444444444444444

<div class="">

<div class="tab w-full overflow-hidden border-t"> 

<div class=" flex flex-col space-y-4">

<div class="tab w-full  text-xl text-black overflow-hidden border-t">

<input class="absolute opacity-0 " id="tab-multi-four" type="checkbox" name="tabs">

<label class="block p-2 text-sm text-left rounded-md bg-green-500   leading-normal cursor-pointer " for="tab-multi-four"><b>運営部</b><span class="text-red-500 text-base font-bold">　New!!</span></label>

<div class="tab-content overflow-hidden border-l-5  leading-normal">

<div class="cp_actab-content">

<p><a name="unei1" id="uneikanri" class="mce-item-anchor"></a></p>

</div>

<br>

<div class="bg-blue-300 bg-opacity-50 p-2 w-full h-full ml-2">

<span class="text-black-600 text-left text-base text-xl font-bold">仕事内容</sapn></div><br>

<div class="bg-amber-200  text-align-left text-left bg-opacity-100 p-2 ml-8"><span class="<text-xl text-left text-black  ml-2 font-bold "></span><ul class="list-disc list-inside "> <li>イリーゼの有料老人ホームと通所サービスの運営全般を行っています</li> <li>各部毎の担当エリアは以下の通りです</li> </ul><br></div></input><br>

<div class="bg-amber-200  text-align-left text-left bg-opacity-100 p-2 ml-8"><span class="<text-xl text-left text-black  ml-2 font-bolddecoration-solid underline ">■運営1課</span>：千葉第1、千葉第2エリア</span><br><span class="<text-xl text-left text-black  ml-2 font-bolddecoration-solid underline ">■運営2課</span>： 埼玉第1、埼玉第2エリア</span><br><span class="<text-xl text-left text-black  ml-2 font-bolddecoration-solid underline">■運営3課</span>： 東京第1・中部、東京第2エリア</span><br><span class="<text-xl text-left text-black  ml-2 font-bolddecoration-solid underline ">■運営4課</span>：神奈川第1・関西、神奈川第2・沖縄エリア</span><br><span class="<text-xl text-left text-black  ml-2 font-bolddecoration-solid underline ">■運営5課</span>：北海道第１、北海道第2・仙台エリア</span><br><span class="<text-xl text-left text-black  ml-2 font-bolddecoration-solid underline ">■運営支援課</span>：運営基準管理チーム、実務研修担当、リスク担当</span><br><span class="<text-xl text-left text-black  ml-2 font-bolddecoration-solid underline ">■運営企画課</span>：新規開設チーム、企画チーム</span></div>

<p><a href="/app/s96da70f606bae585/p99f1ec492869294f/" title="エリア表／エリア長紹介" data-mce-href="/app/s96da70f606bae585/p99f1ec492869294f/"><span style="text-decoration: underline; color: #ff0000;" data-mce-style="text-decoration: underline; color: #ff0000;">※エリア表はこちら</span></a></p>

o﻿k-remain----------------------------------------

<div class="mx-auto overflow-hidden">

        <div class="flex flex-row flex-nowrap">

          <div class="md:shrink-0">

            <figure class="p-8 h-32 w-32"><div class="balloon5"><div class="faceicon"><img src="https://image.jimcdn.com/app/cms/image/transf/dimension=1920x400:format=jpg/path/s96da70f606bae585/image/i00b0a7d01843e515/version/1556067908/image.jpg" class="rounded-full border-2 border-sky-500" /><figcaption class="text-center">金子年長</figcaption></figure>

        </div>

        <div class="">

          <div class="p-5 rounded-md bg-blue-300 bg-opacity-50 text-black">

            <p class="text-lg">こんなことに困ったらお問い合わせください！</p>

          </div>

        </div>

      </div>

    </div><br>

<div class="border-2 border-gray-300 rounded-md outline- 4 p-2 "><ul class="list-disc list-inside "> <li>施設運用ルール、お客様のサービス内容について知りたい</li> </ul><br></div></input><br>

<p class="text-center ...">問い合わせ先<br> ✉Test-new@Test.jp<br> 運営一部 ✉Test-1@Test.jp<br> 運営二部 ✉unei-2@irs.jp<br> 運営三部 ✉Test-3@Test.jp<br> 📞03-6095</p></div></div>

5﻿555555555555555555555555555

<div class="">

<div class="tab w-full overflow-hidden border-t"> 

<div class=" flex flex-col space-y-4">

<div class="tab w-full  text-xl text-black overflow-hidden border-t">

<input class="absolute opacity-0 " id="tab-multi-five" type="checkbox" fname="tabs">

<label class="block p-2 text-sm text-left rounded-md bg-green-500   leading-normal cursor-pointer " for="tab-multi-five"><b>経営管理部</b><span class="text-red-500 text-base font-bold">　New!!</span></label>

<div class="tab-content overflow-hidden border-l-5  leading-normal">

<div class="cp_actab-content">

<p><a name="unei1" id="uneikanri" class="mce-item-anchor"></a></p>

</div>

<br>

<div class="bg-blue-300 bg-opacity-50 p-2 w-full h-full ml-2">

<span class="text-black-600 text-left text-base text-xl font-bold">仕事内容</sapn></div><br>

<div class="bg-amber-200  text-align-left text-left bg-opacity-100 p-2 ml-8"><span class="<text-xl text-left text-black  underline underline-offset-4 ml-2 font-bold ">■経営管理課<br></span> 予算策定や業績管理・分析・各種の実績配信を中心にして、全社の業績・各種経営指標の取り纏めの業務を行っています。また、システム業務も行っております。</span></div><br>

<div class="bg-amber-200  text-align-left text-left bg-opacity-100 p-2 ml-8"><span class="<text-xl text-left text-black  underline underline-offset-4 ml-2 font-bold ">■総務課<br></span>各種管理（施設消耗品、ユニフォーム、押印、車両等）や各種手配（慶弔、出張、名刺等）を行っております。<br>また福祉用具レンタル、販売の管理も行っています。</span></div><br>

<div class="bg-amber-200  text-align-left text-left bg-opacity-100 p-2 ml-8"><span class="<text-xl text-left text-black  underline underline-offset-4 ml-2 font-bold ">■施設修繕課<br></span> 施設の修繕や営繕、保守の管理を行っております。</span></div><br>

<div class="bg-amber-200  text-align-left text-left bg-opacity-100 p-2 ml-8"><span class="<text-xl text-left text-black  underline underline-offset-4 ml-2 font-bold ">■請求課<br></span> <br> 国保連合会とご利用者様への請求・入金に関するサポートと管理に加えて、遅延金管理請求に係る入居契約書・契約解除届の確認、システム関連業務を行っております。</span></div><br>

<div class="bg-amber-200  text-align-left text-left bg-opacity-100 p-2 ml-8"><span class="<text-xl text-left text-black  underline underline-offset-4 ml-2 font-bold ">■情報システム課<br></span> <br> 社内の業務システム・IOTなど導入支援や運用管理を中心に、ITに関する予算や課題管理を行い、各PJの遂行及び進捗管理を行っております。※PCやiPadなどIT機器及びGoogle関係のお問い合わせは、HHD情報システム部までお問い合わせください。</span></div><br>

<div class="bg-blue-300 bg-opacity-50 p-2 w-full h-full ml-2">

<span class="text-black-600 text-left text-base text-xl font-bold">お問い合わせ</sapn></div><br>

<div class="mx-auto overflow-hidden">

        <div class="flex flex-row flex-nowrap">

          <div class="md:shrink-0">

            <figure class="p-8 h-32 w-32"><div class="balloon5"><div class="faceicon"><img src="https://image.jimcdn.com/app/cms/image/transf/dimension=114x114:mode=crop:format=jpg/path/s96da70f606bae585/image/i9df0136268a588f3/version/1557712814/image.jpg" class="rounded-full border-2 border-sky-500" /><figcaption class="text-center">金子年長</figcaption></figure>

        </div>

        <div class="">

          <div class="p-5 rounded-md bg-blue-300 bg-opacity-50 text-black">

            <p class="text-lg">こんなことに困ったらお問い合わせください！</p>

          </div>

        </div>

      </div>

    </div><br>

<div class="border-2 border-gray-300 rounded-md outline- 4 p-2 "><ul class="list-disc list-inside "> <li>自部門の予算・実績に関する内容に関するご質問（主にホーム長以上の皆様）</li> <li>実地指導用の事業所別収支の作成関連のご質問</li> <li>ご入居を検討している方を紹介したい</li> <li>有料サービスの運用ルール、お客様へのサービス内容について知りたい</li> <li>消耗品や備品、ユニフォーム・固定資産等に関するご質問</li><li>稟議書作成や押印関連のご質問</li><li>施設の保守管理、修繕・補修などの手配方法・植栽剪定関連のご質問</li><li>カナミック操作方法、お客様への請求内容等関連のご質問</li><li>CSC・F-revoの操作・入力方法等のご質問</li><li>新たに業務システムを導入・変更に関するご質問</li><li>システムの不具合・機能等に関するご質問</li></ul><br></div></input><br>

<p class="text-center ...">問い合わせ先<br> ✉home-Test@Test.jp<br> 📞03-633-</p></div></div>

6﻿666666666666666666666

<div class="">

<div class="tab w-full overflow-hidden border-t"> 

<div class=" flex flex-col space-y-4">

<div class="tab w-full  text-xl text-black overflow-hidden border-t">

<input class="absolute opacity-0 " id="tab-multi-six" type="checkbox" fname="tabs">

<label class="block p-2 text-sm text-left rounded-md bg-green-500   leading-normal cursor-pointer " for="tab-multi-six"><b>人事部</b><span class="text-red-500 text-base font-bold">　New!!</span></label>

<div class="tab-content overflow-hidden border-l-5  leading-normal">

<div class="cp_actab-content">

<p><a name="unei1" id="uneikanri" class="mce-item-anchor"></a></p>

</div>

<br>