---
title: Case.5 「ライフレンズ」の導入
layout: full-width
categories:
  - new
date: 2022-11-09T05:03:57.748Z
---
<html lang="en"><head><script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"></script><meta charset="UTF-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="viewport" content="width=device-width, initial-scale=1.0"><title>Document</title><link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"><script src="/images/scripts.js"></script><style>body {margin: 10%;margin-left:20%;}#slider {position: relative;width: 1000px;height: 500px;
 overflow: hidden;box-shadow: 0 0 30px rgba(0, 0, 0, 0.3);}#slider ul {position: relative;
list-style: none;height: 100%;width: 10000%;padding: 0;margin: 0;transition: all 750ms ease;left: 0;}#slider ul li {position: relative;height: 100%;float: left;}#slider ul li img{width: 1000px;height: 500px;}#slider #prev, #slider #next {width: 50px;line-height: 50px;
border-radius: 50%;font-size: 2rem;text-shadow: 0 0 20px rgba(0, 0, 0, 0.6);text-align: center;color: white;text-decoration: none;position: absolute;top: 50%;transform: translateY(-50%);transition: all 150ms ease;}#slider #prev:hover, #slider #next:hover {background-color: rgba(0, 0, 0, 0.5);text-shadow: 0;}#slider #prev {left: 10px;}#slider #next {right: 10px;}</style></head>

<span class="text-lg text-left font-bold">Case.5 「ライフレンズ」の導入</span>

<br>

<hr class="border-dashed border-black " />

<br>

![](/images/1628127694.png)

<span class="text-sm text-left">ライフレンズの効果は巡視の業務効率化です。</span>

<span class="text-sm">効率化によって削減された時間を活用するためにオペレーションの見直しを行い、それによって創出された時間をお客様へのサービスとして還元することがテクノロジーの最大の活用効果と考え、取り組んでいます。</span>

![](/images/1628127643.png)

![](/images/1628127701.png)

<span class="text-sm">実際に2020年までに導入した10施設でどのような効果がみられたのか、取り組みの事例をご紹介します。</span>

<br>

![](/images/1628128100.png)

![](/images/1628128120-1-.png)

![](/images/1628128427.png)

<span class="text-sm">業務効率化を実施することで生まれた日中の時間は、お客様サービスに活用することができ、これまで行えなかった質の高いレクや個別に寄り添ったレク・機能訓練などに取り組んだり、カンファレンスの時間として活用したりと、様々な取り組みが生まれています。</span>

<span class="text-xs text-left">＊取り組みの事例をご紹介♬</span>

![](/images/1628128513.png)

<span class="text-sm">普段できないレクを体験することで、お客様に楽しみや喜びを提供でき、新たな一面を知る機会にもなっています。さらにスタッフ側もレクや個別サービスに対する意識が高まり、\
施設の雰囲気がガラッと変わった施設もありました。</span>

![](/images/1628128812.png)

<span class="text-sm">パナソニック社と行っている見守りシステムの共同開発では、現場の皆様のお声を参考にしながら繰り返しアップデートを行ってきました。</span>

<span class="text-sm text-left">その中で、最近新しく追加された機能についてご紹介します。</span>



<br>

<body>

<div id="slider">

<ul id="slideWrap">

<li><img src="/images/1574302761.png" alt=""></li>

<li><img src="/images/1599540407.png" alt=""></li>

<li><img src="/images/1599540773.png" alt=""></li>

<li><img src="/images/image-1-.jpg" alt=""></li>

<li><img src="/images/1599540407.png" alt=""></li>

</ul>

<a id="prev" href="#">&#10094;</a>

<a id="next" href="#">&#10095;</a>

</div>

<br>

<span class="text-xm text-left font-bold ">今後も安心安全な見守りをサポートできるようアップデートを重ねてまいります！！</span>

<br>

![](/images/1628129587.png)

![](/images/1628130418.png)

![](/images/1628130454.png)

<br>

<div class="border-2 border-black text-sm rounded-md outline- 4 p-2 "><ul class="list-disc list-inside ">《これまでの導入施設》<P>東京　　　 ：練馬中村橋・練馬石神井台 <br> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 三鷹深大寺<br> 神奈川　　 ：横浜センター南・横浜三ッ沢<br> 埼玉　　　 ：西大宮・入間・狭山<br> 首都圏以外 ：瑞穂汐路・神戸六甲</span></div></p> </ul><br>

<link href="https://cdn.jsdelivr.net/npm/tailwindcss/dist/tailwind.min.css" rel="stylesheet"> <style>