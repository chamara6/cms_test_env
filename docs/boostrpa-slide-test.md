---
title: boostrpa slide test
layout: full-width
categories:
  - new
date: 2022-12-15T00:07:14.958Z
---
<head><meta charset="UTF-8" /><meta http-equiv="X-UA-Compatible" content="IE=edge" /><meta name="viewport" content="width=device-width, initial-scale=1.0" /><link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"><script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"crossorigin="anonymous"></script><script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js"integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"crossorigin="anonymous"></script><script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js"integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"crossorigin="anonymous"></script><script>$('.carousel').carousel();</script><style>body {/* background: #ccc; */padding-top: 20px;}.carousel-indicators {margin-bottom: -80px;static;}.carousel-indicators button[data-target] {width: 50px;}</style></head>

<!-- Carousel Start --><div id="carouselslider1" class="carousel slide" data-ride="carousel"><div class="carousel-inner align-items-center flex-column"><div class="carousel-item active"><a href="https://www.google.com/"><img src="/images/1574302761.png" class="mx-auto d-block w-50"></a><div class="carousel-caption hidden md:block absolute text-center"><h5 class="text-xl">First slide label</h5><!-- <p>Some representative placeholder content for the first slide.</p> --></div></div><div class="carousel-item"><img src="/images/1574303866.png" class="mx-auto d-block w-50"></div><div class="carousel-item"><img src="/images/1574312367.png" class="mx-auto d-block w-50"></div><div class="carousel-item"><img src="/images/1574314080-1-.png" class="mx-auto d-block w-50 min-w-50 max-w-50"></div></div><!-- Indicator start --><div class="carousel-indicators"><button type="button" data-target="#carouselslider"class="active img-thumbnail" data-slide-to="0"><img src="/images/1574302761.png" alt=""class="d-block w-100"></button><button type="button" data-target="#carouselslider1"class="img-thumbnail" data-slide-to="1"><img src="/images/1574303866.png" alt=""class="d-block w-100"></button><button type="button" data-target="#carouselslider1"class="img-thumbnail" data-slide-to="2"><img src="/images/1574312367.png" alt=""class="d-block w-100"></button><button type="button" data-target="#carouselslider1"class="img-thumbnail" data-slide-to="3"><img src="/images/1574314080-1-.png" alt=""class="d-block w-100"></button></div></div><!-- Indicator Close -->

<br>

<br>

<br>

<br>

<br>

<br>

<br>

![](/images/10.png)

<br>

<br>

<br>

<br>

<div id="carouselslider2" class="carousel slide" data-ride="carousel"><div class="carousel-inner align-items-center flex-column"><div class="carousel-item active"><img src="/images/1599540407.png" class="mx-auto d-block w-50"></div><div class="carousel-item"><img src="/images/1599540773.png" class="mx-auto d-block w-50"></div><div class="carousel-item"><img src="/images/1599540744.png" class="mx-auto d-block w-50"></div><div class="carousel-item"><img src="/images/care-1.png" class="mx-auto d-block w-50 min-w-50 max-w-50"></div></div><!-- Indicator start --><div class="carousel-indicators"><button type="button" data-target="#carouselslider"class="active img-thumbnail" data-slide-to="0"><img src="/images/1599540407.png" alt=""class="d-block w-100"></button><button type="button" data-target="#carouselslider2"class="img-thumbnail" data-slide-to="1"><img src="/images/1599540773.png" alt=""class="d-block w-100"></button><button type="button" data-target="#carouselslider2"class="img-thumbnail" data-slide-to="2"><img src="/images/1599540744.png" alt=""class="d-block w-100"></button><button type="button" data-target="#carouselslider2"class="img-thumbnail" data-slide-to="3"><img src="/images/care-1.png" alt=""class="d-block w-100"></button></div></div><!-- Indicator Close -->

<br>

<br>

<br>

<br>

<br>

![](/images/13.png)

<br>

<br>

<br>

<br>

<br>

<br>

<div id="carouselslider3" class="carousel slide" data-ride="carousel"><div class="carousel-inner align-items-center flex-column"><div class="carousel-item active"><img src="/images/case1-1.pngg" class="mx-auto d-block w-50"></div><div class="carousel-item"><img src="/images/case1-2-slide.png" class="mx-auto d-block w-50"></div><div class="carousel-item"><img src="/images/case1-3.png" class="mx-auto d-block w-50"></div><div class="carousel-item"><img src="/images/case1-4.png" class="mx-auto d-block w-50 min-w-50 max-w-50"></div></div><!-- Indicator start --><div class="carousel-indicators"><button type="button" data-target="#carouselslider"class="active img-thumbnail" data-slide-to="0"><img src="/images/case1-1.png" alt=""class="d-block w-100"></button><button type="button" data-target="#carouselslider3"class="img-thumbnail" data-slide-to="1"><img src="/images/case1-2-slide.png" alt=""class="d-block w-100"></button><button type="button" data-target="#carouselslider3"class="img-thumbnail" data-slide-to="2"><img src="/images/case1-3.png" alt=""class="d-block w-100"></button><button type="button" data-target="#carouselslider3"class="img-thumbnail" data-slide-to="3"><img src="/images/case1-4.png" alt=""class="d-block w-100"></button></div></div><!-- Indicator Close -->

</body>

</html>